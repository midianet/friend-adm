insert into answer_type(method)values('actionJoinMe');
insert into answer_type(method)values('actionJoinMember');
insert into answer_type(method)values('actionDefaultMessage');
insert into answer_type(method)values('actionUpdateComands');
insert into answer_type(method)values('actionSticker');
insert into answer_type(method)values('actionTempo');
insert into answer_type(method)values('actionDollar');
--insert into answer_type(method)values('actionDays');

insert into answer(description,value,type_id)values('Add Bot','Olá amigos me chamo <b>{me}</b>,  estou aqui para agitar o grupo, um abraço a todos 👍',1);
insert into answer(description,value,type_id)values('Add User','Olá <b>{user}</b>, me chamo <b>{me}</b> e quero dar as boas-vindas ao nosso grupo Estamos felizes por você aqui 👏👏👏',2);
insert into answer(description,value,type_id)values('Update','👍',4);
insert into answer(description,value,type_id)values('Mensagem Olá','<b>{user}</b> aqui no grupo ,  muito animado, se anime também Hurrulll 😜😜😜',3);

insert into question(text,type)values('joinme','EQUALS');
insert into question(text,type)values('joinmember','EQUALS');
insert into question(text,type)values('update','EQUALS');
insert into question(text,type)values('ola','EQUALS');

insert into question_answers(question_id,answer_id)values(1,1);
insert into question_answers(question_id,answer_id)values(2,2);
insert into question_answers(question_id,answer_id)values(3,3);
insert into question_answers(question_id,answer_id)values(4,4);



insert into answer(description,value,type_id)values('mint','<b>Linux Mint</b> é uma distribuição baseada no Ubuntu, com o qual é totalmente compatível e partilha os mesmos repositórios. Diferencia-se do Ubuntu por incluir drivers e codecs proprietários por padrão, e por alguns recursos que permitem fazer em modo gráfico configurações que no Ubuntu são feitas em modo texto. Utiliza por padrão o desktop Gnome, modificado. O propósito da distribuição é providenciar um sistema Linux que funcione "out-of-the-box"; isto é, que esteja pronto para uso assim que terminar a instalação. Saiba mais acessando https://www.linuxmint.com/',3);

insert into answer(description,value,type_id)values('debian','<b>Debian</b> é uma das distribuições mais antigas e populares. Ela serviu de base para a criação de diversas outras distribuições igualmente populares, como Ubuntu e Kurumin. Sistema de empacotamento .deb. Sua versão estável é exaustivamente testada, o que a torna ideal para uso em servidores (segurança e estabilidade). Possui um dos maiores repositórios de pacotes dentre as distros (programas pré-compilados disponíveis para se instalar). Saiba mais acessando http://www.debian.org/',3);

insert into answer(description,value,type_id)values('opensuse','<b>openSUSE</b> é a versão livre do belíssimo sistema operacional Novell SuSE. Além de se comportar de forma muito estável e robusta como servidor, também é muito poderoso quando o assunto é desktop. Seu diferencial é o famoso YaST (Yeah Another Setup Tool), um software que centraliza todo o processo de instalação, configuração e personalização do sistema Linux. Podemos dizer que esta é uma das cartas-mestre do SuSE, pois pode se comparar ao painel de controle do Windows. Saiba mais acessando https://www.opensuse.org/',3);

insert into answer(description,value,type_id)values('gentoo','<b>Gentoo</b> é uma metadistribuição baseada no sistema gerenciador de pacotes portage. A nomenclatura usada no desenvolvimento do sistema e seus produtos é inspirada na espécie de pinguim Gentoo. Diferentemente da maioria das distribuições de software, normalmente os pacotes são compilados a partir do código fonte, mantendo a tradição dos ports (em inglês) nos sistemas BSD embora, por conveniência, alguns pacotes grandes são disponibilizados também como binários pré-compilados para várias arquiteturas. saiba mais acessando https://www.gentoo.org/',3);

insert into answer(description,value,type_id)values('slackware','O <b>Slackware</b>, junto com Debian e Red Hat, é uma das distribuições "pai" de todas as outras. Idealizada por Patrick Volkerding, Slack - apelido adotado por sua comunidade de usuários - tem como características principais leveza, simplicidade, estabilidade e segurança. Embora seja considerada por muitos uma distribuição difícil de se usar, voltada para usuário expert ou hacker, possui um sistema de gerenciamento de pacotes simples, assim como sua interface de instalação, que é uma das poucas que continua em modo-texto, mas nem por isso se faz complicada. saiba mais em http://www.slackware.com/',3);

insert into answer(description,value,type_id)values('ubuntu','<b>Ubuntu</b> é uma das distribuições mais populares da atualidade e isso se deve ao fato dela se preocupar muito com o usuário final (desktop). Originalmente baseada no Debian, diferencia-se além do foco no desktop, em sua forma de publicação de novas versões, que são lançadas semestralmente. saiba mais em https://www.ubuntu.com/',3);

insert into answer(description,value,type_id)values('kali linux','<b>Kali Linux</b> é a nova geração da distribuição líder na indústria de testes des intrusão e auditoria de segurança, BackTrack. Kali Linux é uma reconstrução completa a partir do zero, aderindo completamente aos padrões de desenvolvimento Debian. saiba mais em https://www.kali.org/',3);

insert into answer(description,value,type_id)values('Arch Linux','<b>Arch Linux</b> é uma distribuição fundada por Judd Vinet e é otimizada para processadores i686 (Pentium Pro, II etc e AMD compatíveis). Judd inspirou-se em uma distribuição chamada CRUX. Arch é uma distribuição rolling release, ou seja, o sistema é atualizado continuamente. Você obterá acesso às novas versões simplesmente mantendo o sistema atualizado através do gerenciador de pacotes. O número de fiéis usuários tem crescido devido as suas inúmeras qualidades e sua filosofia de desenvolvimento. saiba mais em https://www.archlinux.org/',3);

insert into answer(description,value,type_id)values('centos','<b>CentOS</b> é uma distribuição de classe Enterprise derivada de códigos fonte gratuitamente distribuídos pela Red Hat Enterprise Linux e mantida pelo CentOS Project. CentOS proporciona um grande acesso aos softwares padrão da indústria, incluindo total compatibilidade com os pacotes de softwares preparados especificamente para os sistemas da RHEL. Isso lhe dá o mesmo nível de segurança e suporte, através de updates, que outras soluções Enterprise, porém sem custo. saiba mais em https://www.centos.org/',3);

insert into answer(description,value,type_id)values('freebsd','<b>FreeBSD</b> é um sistema operacional livre do tipo Unix descendente do BSD desenvolvido pela Universidade de Berkeley. Está disponível para as plataformas Intel x86, DEC Alpha, Sparc, PowerPC e PC98 assim como para as arquiteturas baseadas em processadores de 64bits IA-64 e AMD64. Considerado como robusto e estável, geralmente é utilizado em servidores, como de Internet ou Proxies, mas também pode ser utilizado como estação de trabalho. saiba mais em https://www.freebsd.org/',3);

insert into answer(description,value,type_id)values('mageia','<b>Mageia</b> é distribuição de origem francesa criada por ex-funcionários e colaboradores da Mandriva. Ao contrário do Mandriva, que é uma entidade comercial, o projeto Mageia é um projeto da comunidade e uma organização sem fins lucrativos cujo objetivo é desenvolver um sistema operacional livre baseado em Linux. saiba mais em http://www.mageia.org',3);

insert into answer(description,value,type_id)values('trisquel','<b>Trisquel GNU/Linux</b> é uma versão do sistema operacional GNU que utiliza o núcleo Linux-libre e é baseado no Ubuntu. Os principais objetivos do projeto são a produção de um sistema operacional totalmente livre e com suporte a vários idiomas. saiba mais em https://trisquel.info/',3);

insert into answer(description,value,type_id)values('kurumim','<b>Kurumin</b> foi uma distribuição desenvolvida pela equipe do Guia do Hardware e colaboradores, com o objetivo de ser um sistema fácil de usar, voltado especialmente para iniciantes e ex-usuários do Windows. Originalmente o Kurumin foi desenvolvido com base no Knoppix, passando em seguida a utilizar também componentes do Kanotix e outros projetos, além de ser baseado nos pacotes do Debian. Todos os componentes e scripts usados são abertos, o que possibilitou também o surgimento de versões modificadas do sistema. saiba mais em http://www.hardware.com.br/kurumin/',3);

insert into answer(description,value,type_id)values('fedora','<b>Fedora</b> é uma das mais populares e estáveis distribuições existentes na atualidade. Ele era, no começo, um fork para a Comunidade, liberado e mantido pela gigante Red Hat que, na época, estava fechando seu sistema e se concentrando no mercado corporativo. Isso significa que, desde o princípio, o Fedora já contava com o que há de mais moderno em tecnologia de software e, também, com uma das mais competentes e dedicadas equipes atuando em seu desenvolvimento. Se o que você procura é uma distribuição poderosa para criação de um servidor estável, porém com as facilidades das ferramentas de configuração gráficas, ou se, simplesmente, deseja um desktop mais robusto, o Fedora será, certamente, a sua melhor escolha. Saiba mais acessando https://getfedora.org/',3);

insert into answer(description,value,type_id)values('endless','<b>Endless</b> é um sistema operacional baseado em Linux que oferece uma experiência de usuário simplificada e simplificada usando um ambiente de desktop personalizado com o GNOME 3. Ao invés de usar um sistema de gerenciamento de pacotes Linux tradicional, o sistema operacional Endless usa um sistema de arquivos raiz somente leitura gerenciado pelo OSTree com pacotes de aplicativos sobrepostos no topo. Saiba mais em https://endlessos.com/pt-br/',3);
