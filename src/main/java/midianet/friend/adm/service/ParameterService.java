package midianet.friend.adm.service;

import midianet.friend.adm.domain.Parameter;
import midianet.friend.adm.repository.ParameterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ParameterService {

    @Autowired
    private ParameterRepository repository;

    @Transactional
    public Parameter save(Parameter parameter) {
        return repository.save(parameter);
    }

    @Transactional(rollbackOn = {DataIntegrityViolationException.class, Exception.class})
    public void delete(Long id) {
        repository.deleteById(id);
    }

}