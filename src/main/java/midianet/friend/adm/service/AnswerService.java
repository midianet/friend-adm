package midianet.friend.adm.service;

import midianet.friend.adm.domain.Answer;
import midianet.friend.adm.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AnswerService {

    @Autowired
    private AnswerRepository repository;

    @Transactional
    public Answer save(Answer answer) {
        return repository.save(answer);
    }

    @Transactional(rollbackOn = {DataIntegrityViolationException.class, Exception.class})
    public void delete(Long id) {
        repository.deleteById(id);
    }

}