package midianet.friend.adm.service;

import midianet.friend.adm.domain.Task;
import midianet.friend.adm.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class TaskService {

    @Autowired
    private TaskRepository repository;

    @Transactional
    public Task save(Task task) {
        return repository.save(task);
    }

    @Transactional(rollbackOn = {DataIntegrityViolationException.class, Exception.class})
    public void delete(Long id) {
        repository.deleteById(id);
    }

}