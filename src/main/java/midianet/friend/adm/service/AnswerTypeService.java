package midianet.friend.adm.service;

import midianet.friend.adm.domain.AnswerType;
import midianet.friend.adm.repository.AnswerTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AnswerTypeService {

    @Autowired
    private AnswerTypeRepository repository;

    @Transactional
    public AnswerType save(AnswerType type) {
        return repository.save(type);
    }

    @Transactional(rollbackOn = {DataIntegrityViolationException.class, Exception.class})
    public void delete(Long id) {
        repository.deleteById(id);
    }

}