package midianet.friend.adm.service;

import midianet.friend.adm.domain.Question;
import midianet.friend.adm.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class QuestionService {

    @Autowired
    private QuestionRepository repository;

    @Transactional
    public Question save(Question question) {
        return repository.save(question);
    }

    @Transactional(rollbackOn = {DataIntegrityViolationException.class, Exception.class})
    public void delete(Long id) {
        repository.deleteById(id);
    }

}