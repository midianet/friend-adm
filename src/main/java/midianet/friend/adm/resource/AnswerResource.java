package midianet.friend.adm.resource;

import lombok.extern.log4j.Log4j2;
import midianet.friend.adm.domain.Answer;
import midianet.friend.adm.repository.AnswerRepository;
import midianet.friend.adm.service.AnswerService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
@RequestMapping("/api/answers")
public class AnswerResource {

    @Autowired
    private AnswerRepository repository;

    @Autowired
    private AnswerService service;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Answer> list() {
      return repository.findAll(new Sort(Sort.Direction.ASC, "description"));
    }

    @GetMapping(path = "/paginate")
    public DataTableResponse paginate(@RequestParam("draw") Long draw,
                                                      @RequestParam("start") Long start,
                                                      @RequestParam("length") Integer length,
                                                      @RequestParam("search[value]") String searchValue,
                                                      @RequestParam("columns[0][search][value]") String id,
                                                      @RequestParam("columns[1][search][value]") String description,
                                                      @RequestParam("columns[2][search][value]") String type,
                                                      @RequestParam("order[0][column]") Integer order,
                                                      @RequestParam("order[0][dir]") String orderDir) {
        var columns = new String[]{"id", "description", "type"};
        var orderColumns = new String[]{"id", "description", "type.method"};
        var data = new ArrayList<Map<String, Object>>();
        var dt = new DataTableResponse();
        var myId = id.isEmpty() ? null : Long.parseLong(id);
        var myTypeId = type.isEmpty() ? null : Long.parseLong(type);
        dt.setDraw(draw);
        try {
            var qtTotal = repository.count();
            var searchParams = new HashMap<String, String>();
            if (!ObjectUtils.isEmpty(searchValue)) searchParams.put(columns[1], searchValue);
            var page = new Double(Math.ceil(start / length)).intValue();
            var pr = new PageRequest(page, length, new Sort(new Sort.Order(Sort.Direction.fromString(orderDir), orderColumns[order])));
            Page<Answer> list = !id.isEmpty() || !description.isEmpty() || !type.isEmpty() ? repository.findAll(Answer.filter(myId, description, myTypeId), pr) : repository.findAll(pr);
            var qtFilter = list.getTotalElements();
            if (qtFilter > 0)
                list.forEach(e -> {
                    var l = new HashMap<String, Object>();
                    l.put("description", e.getDescription());
                    l.put("DT_RowId", "row_" + e.getId());
                    l.put("type", e.getType().getMethod());
                    l.put("id", e.getId());
                    data.add(l);
                });
            dt.setRecordsFiltered(qtFilter);
            dt.setData(data);
            dt.setRecordsTotal(qtTotal);
        } catch (Exception e) {
            dt.setError("Datatable error " + e.getMessage());
        }
        return dt;
    }

    @GetMapping(path = "/{id}")
    public Answer findById(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Resposta %d", id)));
    }

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public Answer create(@RequestBody Answer answer, HttpServletResponse response) {
        answer.setId(null);
        answer = service.save(answer);
        response.addHeader(HttpHeaders.LOCATION,"/api/answers/" + answer.getId());
        return answer;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    public void update(@PathVariable Long id, @RequestBody Answer answer) {
        var persistent = findById(id);
        BeanUtils.copyProperties(answer,persistent,"id");
        service.save(answer);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        repository.deleteById(id);
    }

}
