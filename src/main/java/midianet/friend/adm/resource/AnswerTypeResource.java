package midianet.friend.adm.resource;

import lombok.extern.log4j.Log4j2;
import midianet.friend.adm.domain.AnswerType;
import midianet.friend.adm.repository.AnswerTypeRepository;
import midianet.friend.adm.service.AnswerTypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
@RequestMapping("/api/answers/types")
public class AnswerTypeResource {

    @Autowired
    private AnswerTypeRepository repository;

    @Autowired
    private AnswerTypeService service;

    @GetMapping
    public List<AnswerType> list() {
        return  repository.findAll(new Sort(Sort.Direction.ASC, "method"));
    }

    @GetMapping(path = "/paginate")
    public DataTableResponse paginate(@RequestParam("draw") Long draw,
                                                      @RequestParam("start") Long start,
                                                      @RequestParam("length") Integer length,
                                                      @RequestParam("search[value]") String searchValue,
                                                      @RequestParam("columns[0][search][value]") String id,
                                                      @RequestParam("columns[1][search][value]") String method,
                                                      @RequestParam("order[0][column]") Integer order,
                                                      @RequestParam("order[0][dir]") String orderDir) {
        var columns = new String[]{"id", "method"};
        var data = new ArrayList<Map<String, Object>>();
        var dt = new DataTableResponse();
        Long myId = id.isEmpty() ? null : Long.parseLong(id);
        dt.setDraw(draw);
        try {
            var qtTotal = repository.count();
            var searchParams = new HashMap<String,String>();
            if (!searchValue.isEmpty())  searchParams.put(columns[1], searchValue);
            var page = Double.valueOf(Math.ceil(start / length)).intValue();
            var pr = new PageRequest(page, length, new Sort(new Sort.Order(Sort.Direction.fromString(orderDir), columns[order])));
            Page<AnswerType> list = !id.isEmpty() || !method.isEmpty() ? repository.findAll(AnswerType.filter(myId, method), pr) : repository.findAll(pr);
            var qtFilter = list.getTotalElements();
            if (qtFilter > 0) {
                list.forEach(e -> {
                    HashMap<String, Object> l = new HashMap<>();
                    l.put("method", e.getMethod());
                    l.put("DT_RowId", "row_" + e.getId());
                    l.put("id", e.getId());
                    data.add(l);
                });
            }
            dt.setRecordsFiltered(qtFilter);
            dt.setData(data);
            dt.setRecordsTotal(qtTotal);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            dt.setError("Datatable error " + e.getMessage());
        }
        return dt;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AnswerType create(@RequestBody AnswerType type, HttpServletResponse response) {
        type.setId(null);
        type = service.save(type);
        response.addHeader(HttpHeaders.LOCATION,"/api/answers/types/" + type.getId());
        return type;
    }

    @GetMapping(path = "/{id}")
    public AnswerType findById(@PathVariable  Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Tipo de Resposta %d", id)));
    }

    @PutMapping(path = "/{id}")
    public void update(@PathVariable Long id, @RequestBody AnswerType type) {
        var persistent = repository.findById(id);
        BeanUtils.copyProperties(type,persistent,"id");
        service.save(type);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }

}