package midianet.friend.adm.resource;

import lombok.extern.log4j.Log4j2;
import midianet.friend.adm.domain.Parameter;
import midianet.friend.adm.repository.ParameterRepository;
import midianet.friend.adm.service.ParameterService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@RestController
@RequestMapping("/api/parameters")
public class ParameterResource {

    @Autowired
    private ParameterRepository repository;

    @Autowired
    private ParameterService service;

    @GetMapping(path = "/paginate")
    public DataTableResponse paginate(@RequestParam("draw")  Long draw,
                                                      @RequestParam("start") Long start,
                                                      @RequestParam("length") Integer length,
                                                      @RequestParam("search[value]") String searchValue,
                                                      @RequestParam("columns[0][search][value]") String id,
                                                      @RequestParam("columns[1][search][value]") String key,
                                                      @RequestParam("order[0][column]") Integer order,
                                                      @RequestParam("order[0][dir]") String orderDir) {
        var columns = new String[]{"id", "key"};
        var data = new ArrayList<Map<String, Object>>();
        var dt = new DataTableResponse();
        Long myId = id.isEmpty() ? null : Long.parseLong(id);
        dt.setDraw(draw);
        try {
            var qtTotal = repository.count();
            var searchParams = new HashMap<String,String>();
            if (!searchValue.isEmpty()) searchParams.put(columns[1], searchValue);
            var page = Double.valueOf(Math.ceil(start / length)).intValue();
            var pr = new PageRequest(page, length, new Sort(new Sort.Order(Sort.Direction.fromString(orderDir), columns[order])));
            Page<Parameter> list = !id.isEmpty() || !key.isEmpty() ? repository.findAll(Parameter.filter(myId, key), pr) : repository.findAll(pr);
            var qtFilter = list.getTotalElements();
            if (qtFilter > 0) {
                list.forEach(e -> {
                    var l = new HashMap<String, Object>();
                    l.put("key", e.getKey());
                    l.put("DT_RowId", "row_" + e.getId());
                    l.put("id", e.getId());
                    data.add(l);
                });
            }
            dt.setRecordsFiltered(qtFilter);
            dt.setData(data);
            dt.setRecordsTotal(qtTotal);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            dt.setError("Datatable error " + e.getMessage());
        }
        return dt;
    }

    @GetMapping(path = "/{id}")
    public Parameter findById(@PathVariable  Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Parâmetro %d", id)));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Parameter create(@RequestBody Parameter parameter, HttpServletResponse response) {
        parameter.setId(null);
        parameter = service.save(parameter);
        response.addHeader(HttpHeaders.LOCATION,"/api/parameters/" + parameter.getId());
        return parameter;
    }

    @PutMapping(path = "/{id}")
    public void update(@PathVariable  Long id, @RequestBody  Parameter parameter) {
        var persistent = repository.findById(id);
        BeanUtils.copyProperties(parameter,persistent,"id");
        service.save(parameter);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable  Long id) {
        repository.deleteById(id);
    }

}