package midianet.friend.adm.resource;

import lombok.extern.log4j.Log4j2;
import midianet.friend.adm.domain.Question;
import midianet.friend.adm.domain.QuestionType;
import midianet.friend.adm.repository.QuestionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
@RequestMapping("/api/questions")
public class QuestionResource {

    @Autowired
    private QuestionRepository repository;

    @GetMapping(path = "/paginate")
    public DataTableResponse paginate(@RequestParam("draw")  Long draw,
                                                      @RequestParam("start")  Long start,
                                                      @RequestParam("length")  Integer length,
                                                      @RequestParam("search[value]")  String searchValue,
                                                      @RequestParam("columns[0][search][value]")  String id,
                                                      @RequestParam("columns[1][search][value]")  String text,
                                                      @RequestParam("order[0][column]")  Integer order,
                                                      @RequestParam("order[0][dir]")  String orderDir) {
        var columns = new String[]{"id", "text"};
        var data = new ArrayList<Map<String, Object>>();
        var dt = new DataTableResponse();
        Long myId = id.isEmpty() ? null : Long.parseLong(id);
        dt.setDraw(draw);
        try {
            var qtTotal = repository.count();
            var searchParams = new HashMap<String, String>();
            if (!ObjectUtils.isEmpty(searchValue)) searchParams.put(columns[1], searchValue);
            var page = new Double(Math.ceil(start / length)).intValue();
            var pr = new PageRequest(page, length, new Sort(new Sort.Order(Sort.Direction.fromString(orderDir), columns[order])));
            Page<Question> list = !id.isEmpty() || !text.isEmpty() ? repository.findAll(Question.filter(myId, text), pr) : repository.findAll(pr);
            var qtFilter = list.getTotalElements();
            if (qtFilter > 0) {
                list.forEach(e -> {
                    var l = new HashMap<String, Object>();
                    l.put("text", e.getText());
                    l.put("DT_RowId", "row_" + e.getId());
                    l.put("id", e.getId());
                    data.add(l);
                });
            }
            dt.setRecordsFiltered(qtFilter);
            dt.setData(data);
            dt.setRecordsTotal(qtTotal);
        } catch (Exception e) {
            //log.error(e);
            dt.setError("Datatable error " + e.getMessage());
        }
        return dt;
    }

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public Question create(@RequestBody Question question, HttpServletResponse response) {
        question.setId(null);
        question.setEnable(true);
        question = repository.save(question);
        response.addHeader(HttpHeaders.LOCATION,"/api/questions/" + question.getId());
        return question;
    }

    @GetMapping(path = "/{id}")
    public Question findById(@PathVariable  Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Pergunta %d", id)));
    }

    @Transactional
    @PutMapping(path = "/{id}")
    public void update(@PathVariable Long id, @RequestBody Question question) {
        var persistent = findById(id);
        BeanUtils.copyProperties(question,persistent,"id");
        repository.save(question);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable  Long id) {
        repository.deleteById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @GetMapping(path = "/types")
    public ResponseEntity<List<Map<String, Object>>> list() {
        return new ResponseEntity(QuestionType.toList(), HttpStatus.OK);

    }

}