package midianet.friend.adm.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DataTableResponse {
    private Long draw;
    private Long recordsTotal;
    private Long recordsFiltered;
    private List data;
    private String error;
}