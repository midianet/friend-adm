package midianet.friend.adm.resource;

import lombok.extern.log4j.Log4j2;
import midianet.friend.adm.domain.Member;
import midianet.friend.adm.repository.MemberRepository;
import midianet.friend.adm.service.MemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
@RequestMapping("/api/members")
public class MemberResource {

    @Autowired
    private MemberRepository repository;

    @Autowired
    private MemberService service;

    @GetMapping
    public List<Member> list() {
        return repository.findAll(new Sort(Sort.Direction.ASC, "name"));
    }

    @GetMapping(path = "/paginate")
    public DataTableResponse paginate(@RequestParam("draw")  Long draw,
                                                      @RequestParam("start")  Long start,
                                                      @RequestParam("length")  Integer length,
                                                      @RequestParam("search[value]")  String searchValue,
                                                      @RequestParam("columns[0][search][value]")  String id,
                                                      @RequestParam("columns[1][search][value]")  String name,
                                                      @RequestParam("order[0][column]")  Integer order,
                                                      @RequestParam("order[0][dir]")  String orderDir) {
        var columns = new String[]{"id", "name"};
        var data = new ArrayList<Map<String, Object>>();
        var dt = new DataTableResponse();
        Long myId = id.isEmpty() ? null : Long.parseLong(id);
        dt.setDraw(draw);
        try {
            var qtTotal = repository.count();
            var searchParams = new HashMap<String,String>();
            if (!searchValue.isEmpty()) searchParams.put(columns[1], searchValue);
            var page = Double.valueOf(Math.ceil(start / length)).intValue();
            var pr = new PageRequest(page, length, new Sort(new Sort.Order(Sort.Direction.fromString(orderDir), columns[order])));
            Page<Member> list = !id.isEmpty() || name.isEmpty() ? repository.findAll(Member.filter(myId, name), pr) : repository.findAll(pr);
            var qtFilter = list.getTotalElements();
            if (qtFilter > 0) {
                list.forEach(e -> {
                    var l = new HashMap<String, Object>();
                    l.put("name", e.getName());
                    l.put("DT_RowId", "row_" + e.getId());
                    l.put("id", e.getId());
                    data.add(l);
                });
            }
            dt.setRecordsFiltered(qtFilter);
            dt.setData(data);
            dt.setRecordsTotal(qtTotal);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            dt.setError("Datatable error " + e.getMessage());
        }
        return dt;
    }

    @GetMapping(path = "/{id}")
    public Member findById(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Membro %d", id)));
    }

    @PostMapping
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public Member create(@RequestBody Member member, HttpServletResponse response) {
        member.setId(null);
        member = service.save(member);
        response.addHeader(HttpHeaders.LOCATION,"/api/members/" + member.getId());
        return member;
    }

    @Transactional
    @PutMapping(path = "/{id}")
    public void update(@PathVariable Long id, @RequestBody  Member member) {
        var persistent = repository.findById(id);
        BeanUtils.copyProperties(member,persistent,"id");
        service.save(member);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable  Long id) {
        repository.deleteById(id);
    }

}