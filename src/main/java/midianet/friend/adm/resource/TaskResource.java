package midianet.friend.adm.resource;

import lombok.extern.log4j.Log4j2;
import midianet.friend.adm.domain.Task;
import midianet.friend.adm.domain.TaskType;
import midianet.friend.adm.repository.TaskRepository;
import midianet.friend.adm.service.TaskService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@RestController
@RequestMapping("/api/tasks")
public class TaskResource {

    @Autowired
    private TaskRepository repository;

    @Autowired
    private TaskService service;

    @GetMapping(path = "/paginate")
    public DataTableResponse paginate(@RequestParam("draw")  Long draw,
                                                      @RequestParam("start")  Long start,
                                                      @RequestParam("length")  Integer length,
                                                      @RequestParam("search[value]")  String searchValue,
                                                      @RequestParam("columns[0][search][value]")  String id,
                                                      @RequestParam("columns[1][search][value]")  String description,
                                                      @RequestParam("columns[2][search][value]")  String type,
                                                      @RequestParam("order[0][column]")  Integer order,
                                                      @RequestParam("order[0][dir]")  String orderDir) {
        var columns = new String[]{"id", "description", "type"};
        var data = new ArrayList<Map<String, Object>>();
        var dt = new DataTableResponse();
        Long myId = id.isEmpty() ? null : Long.parseLong(id);
        dt.setDraw(draw);
        try {
            var qtTotal = repository.count();
            var searchParams = new HashMap<String, String>();
            if (!searchValue.isEmpty()) searchParams.put(columns[1], searchValue);
            var page = Double.valueOf(Math.ceil(start / length)).intValue();
            var pr = new PageRequest(page, length, new Sort(new Sort.Order(Sort.Direction.fromString(orderDir), columns[order])));
            Page<Task> list = !id.isEmpty() || !description.isEmpty() || !type.isEmpty() ? repository.findAll(Task.filter(myId, description, type), pr) : repository.findAll(pr);
            var qtFilter = list.getTotalElements();
            if (qtFilter > 0) {
                list.forEach(e -> {
                    var l = new HashMap<String, Object>();
                    l.put("description", e.getDescription());
                    l.put("type", e.getType().toLabel());
                    l.put("DT_RowId", "row_" + e.getId());
                    l.put("id", e.getId());
                    data.add(l);
                });
            }
            dt.setRecordsFiltered(qtFilter);
            dt.setData(data);
            dt.setRecordsTotal(qtTotal);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            dt.setError("Datatable error " + e.getMessage());
        }
        return dt;
    }

    @GetMapping(path = "/{id}")
    public Task findById(@PathVariable  Long id) {
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Tarefa %d", id)));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Task create(@RequestBody Task task, HttpServletResponse response) {
        task.setId(null);
        task.setEnable(true);
        task = service.save(task);
        response.addHeader(HttpHeaders.LOCATION,"/api/tasks/" + task.getId());
        return task;
    }

    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void update(@PathVariable Long id, @RequestBody Task task) {
        var persistent = findById(id);
        BeanUtils.copyProperties(task,persistent,"id");
        service.save(task);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable  Long id) {
        service.delete(id);
    }

    @GetMapping(path = "/types")
    public List<Map<String, Object>> list() {
        return TaskType.toList();
    }

}