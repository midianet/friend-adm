package midianet.friend.adm.resource;

import lombok.extern.log4j.Log4j2;
import midianet.friend.adm.domain.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/users")
public class UserResource {

    @GetMapping("/list")
    public List<User> list() {
        List<User> list = new ArrayList<>();
        list.add(User.builder().id(1L).name("Teste").build());
        list.add(User.builder().id(2L).name("Outro").build());
        return list;
    }

}