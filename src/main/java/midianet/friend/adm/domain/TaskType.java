package midianet.friend.adm.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum TaskType {
    DAILY, WEEKLY, MONTHLY, YEARLY, SINGLE;

    public static String toLabel(String value) {
        switch (value) {
            case "DAILY":
                return "Diariamente";
            case "WEEKLY":
                return "Semanalmente";
            case "MONTHLY":
                return "Mensalmente";
            case "YEARLY":
                return "Anualmente";
            default:
                return "Uma vez";
        }
    }

    public static List<Map<String, Object>> toList() {
        var list = new ArrayList<Map<String, Object>>();
        for (Enum<TaskType> enumValue : TaskType.values()) {
            var l = new HashMap<String, Object>();
            l.put("value", toLabel(enumValue.name()));
            l.put("id", enumValue.name());
            list.add(l);
        }
        return list;
    }

    @JsonCreator
    public TaskType toEnum(final String value) {
        for (Enum<TaskType> enumValue : TaskType.values()) {
            if (enumValue.toString().equalsIgnoreCase(value)) {
                return (TaskType) enumValue;
            }
        }
        return null;
    }

    public String toLabel() {
        return toLabel(name());
    }

    @JsonValue
    public String toJson() {
        return name();
    }

}