package midianet.friend.adm.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import javax.persistence.*;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnswerType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 50, nullable = false)
    private String method;

    public static Specification<AnswerType> filter(Long id, String method) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            var predicates = new ArrayList<Predicate>();
            if (!ObjectUtils.isEmpty(id))
                predicates.add(criteriaBuilder.equal(root.<Long>get("id"), id));
            if (!ObjectUtils.isEmpty(method))
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("method")), "%" + method.toLowerCase() + "%"));
            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}