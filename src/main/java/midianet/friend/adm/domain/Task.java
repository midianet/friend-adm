package midianet.friend.adm.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import javax.persistence.*;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 50, nullable = false)
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private TaskType type;

    @Column(length = 5, nullable = false)
    private String time;

    @NotNull
    @Column(length = 10, nullable = false)
    private String value;

    private Integer expire;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean enable;

    @ManyToMany
    @JoinTable(name = "task_answers",
            joinColumns = {@JoinColumn(name = "task_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "answer_id", nullable = false)})
    private List<Answer> answers;


    public static Specification<Task> filter(Long id, String description, String type) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            var predicates = new ArrayList<Predicate>();
            if (!ObjectUtils.isEmpty(id))
                predicates.add(criteriaBuilder.equal(root.<Long>get("id"), id));
            if (!ObjectUtils.isEmpty(description))
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("description")), "%" + description.toLowerCase() + "%"));
            if (!ObjectUtils.isEmpty(type)) {
                TaskType t = TaskType.valueOf(type);
                predicates.add(criteriaBuilder.equal(root.get("type"), t));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}