package midianet.friend.adm.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import javax.persistence.*;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(length = 50, nullable = false)
    private String text;

    @ManyToMany
    @JoinTable(name = "question_answers",
            joinColumns = {@JoinColumn(name = "question_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "answer_id", nullable = false)})
    private List<Answer> answers;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(length = 20, nullable = false)
    private QuestionType type;

    private Integer expire;

    @Column(nullable = false, columnDefinition = "boolean default true")
    private Boolean enable;

    public static Specification<Question> filter(Long id, String text) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            var predicates = new ArrayList<Predicate>();
            if (!ObjectUtils.isEmpty(id))
                predicates.add(criteriaBuilder.equal(root.<Long>get("id"), id));
            if (!ObjectUtils.isEmpty(text))
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("text")), "%" + text.toLowerCase() + "%"));
            return criteriaBuilder.and(predicates.toArray(new Predicate[]{}));
        };
    }

}