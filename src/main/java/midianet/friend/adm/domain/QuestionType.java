package midianet.friend.adm.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum QuestionType {
    EQUALS, STARTSWITH, ENDSWITH, CONTAINS;

    public static String toLabel(String value) {
        switch (value) {
            case "STARTSWITH":
                return "Começa com";
            case "ENDSWITH":
                return "Termina com";
            case "CONTAINS":
                return "Contêm";
            default:
                return "Exatamente";
        }
    }

    public static List<Map<String, Object>> toList() {
        var list = new ArrayList<Map<String, Object>>();
        for (Enum<QuestionType> enumValue : QuestionType.values()) {
            var l = new HashMap<String, Object>();
            l.put("value", toLabel(enumValue.name()));
            l.put("id", enumValue.name());
            list.add(l);
        }
        return list;
    }

    @JsonCreator
    public QuestionType toEnum(String value) {
        for (Enum<QuestionType> enumValue : QuestionType.values()) {
            if (enumValue.toString().equalsIgnoreCase(value))
                return (QuestionType) enumValue;
        }
        return null;
    }

    @JsonValue
    public String toJson() {
        return name();
    }

}