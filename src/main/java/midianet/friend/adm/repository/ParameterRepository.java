package midianet.friend.adm.repository;

import midianet.friend.adm.domain.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ParameterRepository extends JpaRepository<Parameter, Long>, JpaSpecificationExecutor {

}